# Development

## Environment

Environments are defined through variables:
- development: for development purpose, logging to console
- test: testing, no console logging
- production: for deployment purpose

## Linting

Lint the code with ESLint.

```bash
npm run lint
```

## Upgrading

Upgrade dependencies using ncu.

```bash
npm run upgrade
```

## Auditing

Audit the code with npm.

```bash
npm audit
```

Run the automated fixing tool.

```bash
npm audit fix
```
