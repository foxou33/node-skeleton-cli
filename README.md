# Node Skeleton CLI

Node.js skeleton command line application.

[![pipeline status](https://gitlab.com/foxou33/node-skeleton-cli/badges/master/pipeline.svg)](https://gitlab.com/foxou33/node-skeleton-cli/commits/master)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes. See deployment for notes on how to deploy the project on a live system.

#### Prerequisites

- Node.js 12.14+
- npm 6.13+

#### Dependencies

* [Yargs](https://yargs.js.org/) - Yargs be a node.js library fer hearties tryin' ter parse optstrings

#### Installing

Install dependencies and build assets.

```bash
npm install
```

#### Running

Run application.

```bash
npm start -- -h
```

#### Logging

Application access logs can be found in `var/log`.

## Development

WIP

#### Debugging

Application use `debug` package.

```bash
DEBUG=* node ./bin/cli
```

#### Testing

Run the automated tests for this system.

```bash
npm test
```

#### Documentation

More information can be found in the [DEVELOPMENT](doc/DEVELOPMENT.md) section of the documentation.

## Deployment

Set the environment variable `NODE_ENV` to production.

```bash
export NODE_ENV=production
```

Install and run the app following standard procedure.

## License

Copyright &copy; 2020 foxou33.

This project is licensed under the GNU GPL v3 License - see the [LICENSE](LICENSE) file for details.
