// Modules.
const childProcess = require('child_process')
const mocha = require('mocha')
const chai = require('chai')
const stripAnsi = require('strip-ansi')
const configSvc = require('../src/services/config')

// Locals.
const describe = mocha.describe
const it = mocha.it
const expect = chai.expect

describe('Test status command', function () {
  it('Should display status information', function (done) {
    childProcess.exec('node ./bin/cli status', {
      cwd: process.cwd()
    }, (err, stdout, stderr) => {
      // eslint-disable-next-line
      expect(err).to.be.null
      expect(stripAnsi(stdout.trim())).to.equal(`version: ${configSvc.version}`)
      // eslint-disable-next-line
      expect(stderr).to.be.empty
      done()
    })
  })
})
