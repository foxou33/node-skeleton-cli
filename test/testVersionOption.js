// Modules.
const childProcess = require('child_process')
const mocha = require('mocha')
const chai = require('chai')
const configSvc = require('../src/services/config')

// Locals.
const describe = mocha.describe
const it = mocha.it
const expect = chai.expect

describe('Test version option', function () {
  it('Should show version number', function (done) {
    childProcess.exec('node ./bin/cli -V', {
      cwd: process.cwd()
    }, (err, stdout, stderr) => {
      // eslint-disable-next-line
      expect(err).to.be.null
      expect(stdout.trim()).to.equal(`${configSvc.version}`)
      // eslint-disable-next-line
      expect(stderr).to.be.empty
      done()
    })
  })
})
