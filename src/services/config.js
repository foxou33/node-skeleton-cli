// Modules.
const path = require('path')
const pkgInfo = require('../../package')

// Globals.
exports.appName = 'Skeleton CLI'
exports.version = pkgInfo.version
exports.copyright = 'Copyright © ' + new Date().getFullYear() + ' ' + exports.appName + '. All rights reserved.'
exports.applicationLog = path.join(process.cwd(), './var/log/application.log')
