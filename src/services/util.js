// Modules.
const configSvc = require('./config')
const debug = require('debug')(`${configSvc.appName}:util`)
const winston = require('winston')
const prettyjson = require('prettyjson')

// Globals.
// noinspection JSCheckFunctionSignatures
exports.logger = winston.createLogger({
  level: 'silly',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.printf(info => `${info.timestamp}: ${info.level}: ${info.message}`)
      )
    }),
    new winston.transports.File({
      filename: configSvc.applicationLog
    })
  ]
})
exports.argv = null

/**
 * Init.
 */
exports.init = () => {
  // noinspection JSUnresolvedFunction
  exports.argv = require('yargs/yargs')(process.argv.slice(2))
    .commandDir('../commands')
    .demandCommand()
    .usage(configSvc.appName + '\n\nUsage: $0 <command> [options]')
    .help('help').alias('help', 'h')
    .version('version', configSvc.version).alias('version', 'V')
    .epilog(configSvc.copyright)
    .argv

  debug('argv: %O', exports.argv)
}

/**
 * Exit error.
 *
 * @param {string} message
 * @param {number} code
 */
exports.exitError = (message, code) => {
  exports.logger.error(message)
  exports.logger.on('finish', () => {
    process.exit(code)
  })
  exports.logger.end()
}

/**
 * Dump data.
 *
 * @param {object} data
 */
exports.dump = (data) => {
  console.info(prettyjson.render(data))
}
