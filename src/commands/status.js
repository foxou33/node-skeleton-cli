// Modules.
const utilSvc = require('../services/util')
const configSvc = require('../services/config')

/**
 * Command.
 *
 * @type {string}
 */
exports.command = 'status'

/**
 * Description.
 *
 * @type {string}
 */
exports.desc = 'Display status information'

/**
 * Options.
 *
 * @type {{}}
 */
exports.builder = {}

/**
 * Handler.
 *
 * @type {function}
 */
exports.handler = () => {
  utilSvc.dump({
    version: configSvc.version
  })
}
